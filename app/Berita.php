<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';
    protected $primaryKey = 'id';
    protected $fillable = ['judul_berita', 'penulis_berita', 'isi_berita', 'gambar_berita', 'tanggal_berita'];
}
