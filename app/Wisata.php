<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wisata extends Model
{
    protected $table = 'wisata';
    protected $primaryKey = 'id';
    protected $fillable = ['gambar_wisata', 'judul_wisata', 'tempat_wisata', 'ringkasan', 'maps'];
}
