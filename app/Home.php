<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    public function getCountData()
    {
    	return [
    		'berita'         => DB::table('berita')->count(),
			'event'         => DB::table('event')->count(),
			'transaksi'         => DB::table('transaksi')->count(),
			'kuliner'         => DB::table('kuliner')->count(),
			'wisata'         => DB::table('wisata')->count(),
            // 'user'         => DB::table('user')->count(),
    	];
    }
}
