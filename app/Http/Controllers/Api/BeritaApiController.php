<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Berita;

class BeritaApiController extends Controller
{
    public function index(){
        $berita_list = Berita::all();
        return response($berita_list);
    }

    public function detail_berita($id){
        $berita = Berita::find($id);
        return response($berita);
    }
}
