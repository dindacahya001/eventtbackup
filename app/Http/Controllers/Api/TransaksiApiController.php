<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaksi;

class TransaksiApiController extends Controller
{
    public function index(){
        $transaksi_list = Transaksi::all();
        return response($transaksi_list);
    }

    public function create(Request $request) {
        $transaksi = new Transaksi;
        $transaksi->event_id = $request->event_id;
        $transaksi->tot_harga = $request->tot_harga;
        $transaksi->gambar = "default.jpg";
        $transaksi->save();
  
        return response()->json([
            'succsess' => 'Data berhasil diupdate',
            'data' => $transaksi
        ], 200);
    }

    public function update(Request $request, $id){
        $transaksi::find($id);
        $transaksi->update($request->all());
        return response()->json([
            'succsess' => 'Data berhasil diupdate',
            'data' => $transaksi
        ], 200);
    }

    public function delete($id)
    {
        $transaksi = Transaksi::find($id);
        $transaksi->delete();
        
        return response()->json([
            'succsess' => 'Data berhasil diupdate',
            'data' => $transaksi
        ], 200);
        
    }
}
