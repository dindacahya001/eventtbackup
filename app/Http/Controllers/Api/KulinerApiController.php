<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kuliner;

class KulinerApiController extends Controller
{
    public function index(){
        $kuliner_list = Kuliner::all();
        return response($kuliner_list);
    }
    public function detail_kuliner($id){
        $kuliner = Kuliner::find($id);
        return response($kuliner);
    }
}
