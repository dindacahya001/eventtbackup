<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Wisata;

class WisataApiController extends Controller
{
    public function index(){
        $wisata_list = Wisata::all();
        return response($wisata_list);
    }
    public function detail_wisata($id){
        $wisata = Wisata::find($id);
        return response($wisata);
    }
}
