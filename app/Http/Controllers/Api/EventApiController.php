<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;

class EventApiController extends Controller
{
    public function index(){
        $event_list = Event::all();
        return response($event_list);
    }
    public function detail_event($id){
        $event = Event::find($id);
        return response($event);
    }

}
