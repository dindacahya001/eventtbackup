<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Iklan;

class IklanApiController extends Controller
{
    public function index(){
        $iklan_list = Iklan::all();
        return response($iklan_list);
    }
    public function detail_iklan($id){
        $iklan = Iklan::find($id);
        return response($iklan);
    }
}
