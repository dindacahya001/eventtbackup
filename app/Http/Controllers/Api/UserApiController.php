<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserApiController extends Controller
{
    public function index(){
        $user_list = User::all();
        return response($user_list);
    }

    public function create(Request $request) {
        $user = new User;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->name = $request->name;
        $user->gambar = "default.jpg";
        $user->save();
  
        return response()->json([
            'succsess' => 'Data berhasil diupdate',
            'data' => $user
        ], 200);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        // $user->email = $request->email;
        // $user->password = $request->password;
        // $user->name = $request->name;
        // $user->gambar = "default.jpg";
        // $user->save();
        $user->update($request->all());
        return response()->json([
            'succsess' => 'Data berhasil diupdate',
            'data' => $user
        ], 200);
    }
}
