<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

use App\Iklan;
use Storage;
use Validator;
use App\Http\Controllers\Controller;

class IklanController extends Controller
{
    public function __construct(Request $req) {
        $this->request = $req;
    }

    public function create(){
        return view('cp/iklan/create_iklan');
    }
    public function show(){
        $iklan_list = Iklan::paginate(3);
        return view('cp/iklan/show_iklan', compact('iklan_list'));
    }
    public function detail_iklan($id){
        $iklan = Iklan::find($id);
        return view('cp/iklan/detail_iklan', compact('iklan'));
    }

    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'judul_iklan' => 'required|max:100|String',
            'gambar_iklan' => 'required|image|max:4000',
            'ringkasan' => 'required|String',
        ]);

        //picture
        if($request->hasFile('gambar_iklan')){
            $gambar_iklan = $request->file('gambar_iklan');
            $ext = $gambar_iklan->getClientOriginalExtension();
            if($request->file('gambar_iklan')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/iklanupload';
                $request->file('gambar_iklan')->move($upload_path, $picture_name);
                $input['gambar_iklan'] = $picture_name;
            }
        }

        $iklan = new Iklan($input);

        if ($iklan->save()) {
            return redirect('cp\show_iklan')->with('sukses','Data Berhasil diinput');
        }
    }
        
    public function edit($id){
        $iklan = Iklan::findOrFail($id);
        return view('cp/iklan/edit', compact('iklan'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if($request->hasFile('gambar_iklan')){
            $gambar_iklan = $request->file('gambar_iklan');
            $ext = $gambar_iklan->getClientOriginalExtension();
            if($request->file('gambar_iklan')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/iklanupload';
                $request->file('gambar_iklan')->move($upload_path, $picture_name);
                $input['gambar_iklan'] = $picture_name;
            }
        }
            
        $iklan = Iklan::find($id);
        if ($iklan->update($input)) {
            return redirect('cp\show_iklan')->with('sukses','Data Berhasil diperbarui');
        }
    }

    public function delete($id){
        $iklan = Iklan::findOrFail($id);
        $iklan->delete();
        return redirect('cp\show_iklan')->with('sukses','Data Berhasil dihapus');
    }
}

