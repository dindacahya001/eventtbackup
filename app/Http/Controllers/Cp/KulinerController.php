<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

use App\Kuliner;
use Storage;
use Validator;
use App\Http\Controllers\Controller;

class KulinerController extends Controller
{
    public function __construct(Request $req) {
        $this->request = $req;
    }

    public function create(){
        return view('cp/kuliner/create_kuliner');
    }
    public function show(){
        $kuliner_list = Kuliner::paginate(3);
        return view('cp/kuliner/show_kuliner', compact('kuliner_list'));
    }
    public function detail_kuliner($id){
        $kuliner = Kuliner::find($id);
        return view('cp/kuliner/detail_kuliner', compact('kuliner'));
    }

    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'judul_kuliner' => 'required|max:100|String',
            'tempat_kuliner' => 'required|max:100|String',
            'maps' => 'required',
            'gambar_kuliner' => 'required|image|max:4000',
            'ringkasan' => 'required|String',
        ]);

        //picture
        if($request->hasFile('gambar_kuliner')){
            $gambar_kuliner = $request->file('gambar_kuliner');
            $ext = $gambar_kuliner->getClientOriginalExtension();
            if($request->file('gambar_kuliner')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/kulinerupload';
                $request->file('gambar_kuliner')->move($upload_path, $picture_name);
                $input['gambar_kuliner'] = $picture_name;
            }
        }

        $kuliner = new Kuliner($input);

        if ($kuliner->save()) {
            return redirect('cp\show_kuliner')->with('sukses','Data Berhasil diinput');
        }
    }
        
    public function edit($id){
        $kuliner = Kuliner::findOrFail($id);
        return view('cp/kuliner/edit', compact('kuliner'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if($request->hasFile('gambar_kuliner')){
            $gambar_kuliner = $request->file('gambar_kuliner');
            $ext = $gambar_kuliner->getClientOriginalExtension();
            if($request->file('gambar_kuliner')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/kulinerupload';
                $request->file('gambar_kuliner')->move($upload_path, $picture_name);
                $input['gambar_kuliner'] = $picture_name;
            }
        }
            
        $kuliner = Kuliner::find($id);
        if ($kuliner->update($input)) {
            return redirect('cp\show_kuliner')->with('sukses','Data Berhasil diperbarui');
        }
    }

    public function delete($id){
        $kuliner = Kuliner::findOrFail($id);
        $kuliner->delete();
        return redirect('cp\show_kuliner')->with('sukses','Data Berhasil dihapus');
    }
}
