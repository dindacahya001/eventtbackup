<?php

namespace App\Http\Controllers\Cp;
use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function create(){
        return view('cp/user/create');
    }
    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'name' => 'required|max:100|String',
            'email' => 'required|max:100|String',
            'password' => 'required',
            'gambar' => 'required|image|max:4000',
        ]);

        //picture
        if($request->hasFile('gambar')){
            $picture_user = $request->file('gambar');
            $ext = $picture_user->getClientOriginalExtension();
            if($request->file('gambar')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/userupload';
                $request->file('gambar')->move($upload_path, $picture_name);
                $input['gambar'] = $picture_name;
            }
        }

        $user['point'] = 0;
        $user = new User($input);
        $user->password = bcrypt($request->get('password'));

        if ($user->save()) {
            return redirect('cp\show_user')->with('sukses','Data Berhasil diinput');
        }
    }
    public function show(){
        $user_list = User::paginate(3);
        return view('cp/user/show', compact('user_list'));
    }

    public function edit($id){
        $user = User::findOrFail($id);
        return view('cp/user/edit', compact('user'));
    }

    public function update(Request $request, $id) {
        // $input = $request->all();

        // $this->validate($request,[
        //     'desc' => 'required',
        //      ]);      
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if($request->hasFile('gambar')){
            $picture_user = $request->file('gambar');
            $ext = $picture_user->getClientOriginalExtension();
            if($request->file('gambar')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/userupload';
                $request->file('gambar')->move($upload_path, $picture_name);
                $user->gambar = $picture_name;
            }
        }
        if ($request->password!=null){
            $user->password = bcrypt($request['password']);
        
        }else{
            $input['password'] =  $user->password;
        }

        if ($user->update()) {
            return redirect('cp\show_user')->with('sukses','Data Berhasil diperbarui');
        }
    }

    public function delete($id){
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('cp\show_user')->with('sukses','Data Berhasil dihapus');
    }

    public function detail($id){
        $user = User::find($id);
        return view('cp/user/detail', compact('user'));
    }
}
