<?php
namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;
use App\Home;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function index()
     {
         return view('layouts.dashboard');
     }
}
