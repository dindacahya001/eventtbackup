<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

use App\Berita;
use Storage;
use Validator;
use App\Http\Controllers\Controller;

class BeritaController extends Controller
{
    public function __construct(Request $req) {
        $this->request = $req;
    }

    public function create_berita(){
        return view('cp/berita/create_berita');
    }
    public function show_berita(){
        $berita_list = Berita::paginate(3);
        return view('cp/berita/show_berita', compact('berita_list'));
    }

    public function detail_berita($id){
        $berita = Berita::find($id);
        return view('cp/berita/detail_berita', compact('berita'));
    }

    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'gambar_berita' => 'required',
            'judul_berita' => 'required',
            'penulis_berita' => 'required',
            'tanggal_berita' => 'required',
            'isi_berita' => 'required'
        ]);

        //picture
        if($request->hasFile('gambar_berita')){
            $gambar_berita = $request->file('gambar_berita');
            $ext = $gambar_berita->getClientOriginalExtension();
            if($request->file('gambar_berita')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/beritaupload';
                $request->file('gambar_berita')->move($upload_path, $picture_name);
                $input['gambar_berita'] = $picture_name;
            }
        }

        $berita = new Berita($input);

        if ($berita->save()) {
            return redirect('cp\show_berita')->with('sukses','Data Berhasil diinput');
        }
    }
        
    public function edit($id){
        $berita = Berita::findOrFail($id);
        return view('cp/berita/edit', compact('berita'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if($request->hasFile('gambar_berita')){
            $gambar_berita = $request->file('gambar_berita');
            $ext = $gambar_berita->getClientOriginalExtension();
            if($request->file('gambar_berita')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/beritaupload';
                $request->file('gambar_berita')->move($upload_path, $picture_name);
                $input['gambar_berita'] = $picture_name;
            }
        }
            
        $berita = Berita::find($id);
        if ($berita->update($input)) {
            return redirect('cp\show_berita')->with('sukses','Data Berhasil diperbarui');
        }
    }

    public function delete($id){
        $berita = Berita::findOrFail($id);
        $berita->delete();
        return redirect('cp\show_berita')->with('sukses','Data Berhasil dihapus');
    }
}
