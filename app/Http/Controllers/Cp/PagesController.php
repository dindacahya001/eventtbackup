<?php

namespace App\Http\Controllers\Cp;

// use App\Berita;
// use App\Event;
// use App\Kuliner;
// use App\Transaksi;
// use App\User;
// use App\Wisata;
use App\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    // Public function index(){
    //     $berita = Berita::all();
    //     $wisata = Wisata::all();
    //     $kuliner = Kuliner::all();
    //     $transaksi = Transaksi::all();
    //     $user = User::all();
    //     $event = Event::all();
    //     return view('layouts.dashboard', compact('berita', 'wisata', 'kuliner', 'transaksi', 'user', 'event'));
    // }

    public function index(Home $home)
    {
        return view('layouts.dashboard', [
            'count' => $home->getCountData()
        ]);
    }
}
