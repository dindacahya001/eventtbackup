<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

use App\Wisata;
use Storage;
use Validator;
use App\Http\Controllers\Controller;

class WisataController extends Controller
{
    public function __construct(Request $req) {
        $this->request = $req;
    }

    public function create(){
        return view('cp/wisata/create_wisata');
    }
    public function show(){
        $wisata_list = Wisata::paginate(2);
        return view('cp/wisata/show_wisata', compact('wisata_list'));
    }
    public function detail_wisata($id){
        $wisata = Wisata::find($id);
        return view('cp/wisata/detail_wisata', compact('wisata'));
    }


    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'judul_wisata' => 'required|max:100|String',
            'tempat_wisata' => 'required|max:100|String',
            'maps' => 'required',
            'gambar_wisata' => 'required|image|max:4000',
            'ringkasan' => 'required|String',
        ]);

        //picture
        if($request->hasFile('gambar_wisata')){
            $gambar_wisata = $request->file('gambar_wisata');
            $ext = $gambar_wisata->getClientOriginalExtension();
            if($request->file('gambar_wisata')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/wisataupload';
                $request->file('gambar_wisata')->move($upload_path, $picture_name);
                $input['gambar_wisata'] = $picture_name;
            }
        }

        $wisata = new Wisata($input);

        if ($wisata->save()) {
            return redirect('cp\show_wisata')->with('sukses','Data Berhasil diinput');
        }
    }
        
    public function edit($id){
        $wisata = Wisata::findOrFail($id);
        return view('cp/wisata/edit', compact('wisata'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if($request->hasFile('gambar_wisata')){
            $gambar_wisata = $request->file('gambar_wisata');
            $ext = $gambar_wisata->getClientOriginalExtension();
            if($request->file('gambar_wisata')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/wisataupload';
                $request->file('gambar_wisata')->move($upload_path, $picture_name);
                $input['gambar_wisata'] = $picture_name;
            }
        }
            
        $wisata = Wisata::find($id);
        if ($wisata->update($input)) {
            return redirect('cp\show_wisata')->with('sukses','Data Berhasil diperbarui');
        }
    }

    public function delete($id){
        $wisata = Wisata::findOrFail($id);
        $wisata->delete();
        return redirect('cp\show_wisata')->with('sukses','Data Berhasil dihapus');
    }
}
