<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Auth;
use Validator;

class TransaksiController extends Controller
{
    public function create(){
        $list_event = Event::pluck('judul_event', 'id');
        return view('cp/transaksi/create', compact('list_event'));
    }
    public function store(Request $request){
        $input = $request->all();
        $user_id = Auth::user()->id;

        if($request->hasFile('bukti')){
            $picture_post= $request->file('bukti');
            $ext = $picture_post->getClientOriginalExtension();
            if($request->file('bukti')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/transaksiupload';
                $request->file('bukti')->move($upload_path, $picture_name);
                $input['bukti'] = $picture_name;
            }
        }
        
        $transaksi = new Transaksi($input);
        $transaksi['user_id'] = $user_id;
        $transaksi['tot_harga'] = 0;

        if ($transaksi->save()) {
            return redirect('cp\show_transaksi')->with('sukses','Data Berhasil diinput');
        }
    }
    public function show(){
        $transaksi_list = Transaksi::paginate(3);
        return view('cp/transaksi/show', compact('transaksi_list'));
    }
    public function delete($id){
        $transaksi = Transaksi::findOrFail($id);
        $transaksi->delete();
        return redirect('cp\show_transaksi')->with('sukses','Data Berhasil dihapus');
    }
    public function detail($id){
        $transaksi = Transaksi::find($id);
        return view('cp/transaksi/detail', compact('transaksi'));
    }
}
