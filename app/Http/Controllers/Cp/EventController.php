<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

use App\Event;
use Storage;
use Validator;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function __construct(Request $req) {
        $this->request = $req;
    }

    public function create(){
        return view('cp/event/create_event');
    }
    public function show(){
        $event_list = Event::paginate(3);
        return view('cp/event/show_event', compact('event_list'));
    }
    public function detail_event($id){
        $event = Event::find($id);
        return view('cp/event/detail_event', compact('event'));
    }

    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'judul_event' => 'required|max:100|String',
            'tempat_event' => 'required|max:100|String',
            'tanggal_event' => 'required|max:100|String',
            'waktu_event' => 'required',
            'maps' => 'required',
            'harga' => 'required',
            'gambar_event' => 'required|image|max:4000',
            'tata_cara' => 'required|String',
            'ringkasan' => 'required|String',
        ]);

        //picture
        if($request->hasFile('gambar_event')){
            $gambar_event = $request->file('gambar_event');
            $ext = $gambar_event->getClientOriginalExtension();
            if($request->file('gambar_event')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/eventupload';
                $request->file('gambar_event')->move($upload_path, $picture_name);
                $input['gambar_event'] = $picture_name;
            }
        }

        $event = new Event($input);

        if ($event->save()) {
            return redirect('cp\show_event')->with('sukses','Data Berhasil diinput');
        }
    }
        
    public function edit($id){
        $event = Event::findOrFail($id);
        return view('cp/event/edit', compact('event'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if($request->hasFile('gambar_event')){
            $gambar_event = $request->file('gambar_event');
            $ext = $gambar_event->getClientOriginalExtension();
            if($request->file('gambar_event')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/eventupload';
                $request->file('gambar_event')->move($upload_path, $picture_name);
                $input['gambar_event'] = $picture_name;
            }
        }
            
        $event = Event::find($id);
        if ($event->update($input)) {
            return redirect('cp\show_event')->with('sukses','Data Berhasil diperbarui');
        }
    }

    public function delete($id){
        $event = Event::findOrFail($id);
        $event->delete();
        return redirect('cp\show_event')->with('sukses','Data Berhasil dihapus');
    }
}
