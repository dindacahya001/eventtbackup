<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Event extends Model
{
    protected $table = 'event';
    protected $primaryKey = 'id';
    protected $fillable = ['judul_event', 'category', 'tempat_event', 'tanggal_event', 'waktu_event', 'tata_cara', 'gambar_event', 'harga', 'ringkasan', 'maps'];

    public function transaksi(){
        return $this->hasMany('App\Transaksi', 'event_id');
    }

}
