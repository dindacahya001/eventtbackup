<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $primaryKey = 'id';
    protected $fillable = ['event_id', 'user_id', 'bukti','tot_harga', 'status'];

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }
}
