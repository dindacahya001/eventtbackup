<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kuliner extends Model
{
    protected $table = 'kuliner';
    protected $primaryKey = 'id';
    protected $fillable = ['gambar_kuliner', 'judul_kuliner', 'tempat_kuliner', 'ringkasan', 'maps'];
}
