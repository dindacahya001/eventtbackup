<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table = 'iklan';
    protected $primaryKey = 'id';
    protected $fillable = ['gambar_iklan', 'judul_iklan', 'ringkasan'];
}
