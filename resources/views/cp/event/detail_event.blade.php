
@extends('layouts/master')

@section('content')
<div id="event">

<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Detail Event</h3>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Judul Event</th>
                        <td>{{ $event->judul_event}}</td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <td>{{ $event->tanggal_event}}</td>
                    </tr>
                    <tr>
                        <th>Waktu</th>
                        <td>{{ $event->waktu_event}}</td>
                    </tr>
                    <tr>
                        <th>Kategori</th>
                        <td>{{ $event->category}}</td>
                    </tr>
                    <tr>
                        <th>Tempat</th>
                        <td>{{ $event->tempat_event}}</td>
                    </tr>
                    <tr>
                        <th>Harga</th>
                        <td>{{ $event->harga}}</td>
                    </tr>
                    <tr>
                        <th>Ringkasan</th>
                        <td>{{ $event->ringkasan}}</td>
                    </tr>
                    <tr>
                        <th>Tata Cara</th>
                        <td>{{ $event->tata_cara}}</td>
                    </tr>
                    <tr>
                    <th>Gambar</th>
                    <td>
                        <a href="{{ asset('/public/eventupload/'.$event->gambar_event) }}"  data-toggle="lightbox" >
                        <img id="preview_photo" src="{{ asset('/public/eventupload/'.$event->gambar_event) }}"  class="img-fluid mb-2" alt="white sample"/>
                    </td>
                    </tr>
                    <tr>
                    <th>Maps</th>
                    <td>
                        <div class="border-top-0" style="text-align:center" height="50px">
                        {!! $event->maps!!}
                        </div>
                    </td>
                    </tr>
                    <tr>    
                    </thead>
                  </table>
                </div>
                <div class="card-footer text-right">
                <a class="btn btn-primary" href="{{ url('cp/show_event') }}">
                Back
            </a>
        </div>
            </div>
          </section>
@stop