@extends('layouts/master')
@section('content')

<form action="{{ url('cp/event/save') }}" method="POST" enctype="multipart/form-data">
@csrf
<body>

    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="page-title mb-0 p-0">Tambah Data Event</h3>
                    </div>
                </div>
            </div>
            @include('errors/form_error')
            <div class="container-fluid">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal form-material mx-2">
                                    <div class="form-group">
                                        <label for="judul_event" class="col-md-12 mb-0">Judul Event</label>
                                        <div class="col-md-12">
                                            <input type="text" name="judul_event" value="{{ old('judul_event') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_event" class="col-md-12 mb-0">Tempat</label>
                                        <div class="col-md-12">
                                            <input type="text" name="tempat_event" value="{{ old('tempat_event') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_event" class="col-md-12 mb-0">Tanggal</label>
                                        <div class="col-md-12">
                                            <input type="date" name="tanggal_event" value="{{ old('tanggal_event') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="waktu_event">Waktu Event</label>
                                        <input type="time" name="waktu_event" value="{{ old('waktu_event') }}" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label  for="gambar_event" class="col-sm-6 control-label">Gambar</label>
                                        <img id="preview_photo" class="img-fluid md-3 mb-3"  />
                                        <div class="col-md-12">
                                        <input type="file" accept="image/*" name="gambar_event" onchange="preview(event)"  value="{{ old('gambar_event') }}" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori</label>
                                        <select class="form-control" name="category" >
                                            <option value="rekomendasi">Rekomendasi </option>
                                            <option value="update">Update </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="harga" class="col-md-12 mb-0">Harga</label>
                                        <div class="col-md-12">
                                            <input type="text" name="harga" value="{{ old('harga') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="maps">Maps</label>
                                            <div class="peta-responsive">
                                            <textarea name="maps" ng-model="event.maps" class="form-control" rows="3" ></textarea>
                                            </div>             
                                        </div>
                                    <div class="form-group">
                                        <label for="tata_cara" class="col-md-12 mb-0">Tata Cara</label>
                                        <div class="col-md-12">
                                            <textarea name="tata_cara" rows="5" class="form-control ps-0 form-control-line"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ringkasan" class="col-md-12 mb-0">Ringkasan</label>
                                        <div class="col-md-12">
                                            <textarea name="ringkasan" rows="5" class="form-control ps-0 form-control-line"></textarea>
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <div class="col-sm-12 d-flex">
                                            <input class="btn btn-success mx-auto mx-md-0 text-white" type="submit" value="Submit" class="primary" />
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>        
        </div>
    </div>


</body>
</form>
@stop

{{-- JS untuk menampilkam photo profile --}}
<script type="text/javascript">
        function preview(event){
            var input = event.target.files[0];
            var reader = new FileReader();

            reader.onload = function(){
                var result = reader.result;
                var preview_photo = document.getElementById('preview_photo');
                preview_photo.src = result
            }

            reader.readAsDataURL(input);
        }
    </script>