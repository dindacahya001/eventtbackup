@extends('layouts/master')

@section('content')
<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="page-title mb-0 p-0">Data Event</h3>
                    </div>
                </div>
                <div class="card-header">
                    <a class="btn btn-success add-row" href="create_event">
                        Tambah Data Baru <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!-- column -->                   
                <div class="card">
                    <div class="col-sm-12">
                    @if (!empty($event_list))
                        <div class="card">
                        @if (session('sukses'))
                        <div class="alert alert-warning" role="alert">
                            {{session('sukses')}}
                        </div>
                            @endif
                            <div class="card-body">
                            @if (!empty($event_list))
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0" style="text-align:center" width="120px">Judul Event</th>
                                                <th class="border-top-0" style="text-align:center" width="40px">Tanggal Event</th>
                                                <th class="border-top-0" style="text-align:center" width="80px">Waktu Event</th>
                                                <th class="border-top-0" style="text-align:center" width="40px">Kategori</th>
                                                <th class="border-top-0" style="text-align:center" width="150px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($event_list as $event)
                                            <tr>
                                                <td>{{ $event->judul_event}}</td>
                                                <td style="text-align:center">{{ $event->tanggal_event}}</td>
                                                <td style="text-align:center">{{ $event->waktu_event}}</td>
                                                <td style="text-align:center"> {{ $event->category}} </td>
                                                <td style="text-align:center" class="project-actions text-right"> 
                                                    <a class="btn btn-primary btn-sm" href="{{ url('cp/detail_event/'.$event->id) }}">
                                                        <i class="fas fa-folder">
                                                        </i>
                                                        Detail
                                                    </a>
                                                <a class="btn btn-info btn-sm" href="{{ url('cp/edit_event/'.$event->id.'/edit')}}">
                                                    <i class="fas fa-pencil-alt">
                                                    </i>
                                                    Edit
                                                </a>
                                                <a class="btn btn-danger btn-sm" href ="{{ url('cp/hapus_event/'.$event->id) }}" >
                                                    <i class="fas fa-trash">
                                                    </i>
                                                    Delete
                                                </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="card-footer clearfix">
                                        <ul class="pagination pagination-sm m-0 float-right">
                                        <div class="paging">
                                        {{ $event_list->links() }}
                                         </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@stop   

