
@extends('layouts/master')

@section('content')
<div id="berita">

<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Detail Berita</h3>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                    <th>Judul Berita</th>
                    <td>{{ $berita->judul_berita}}</td>
                    </tr>
                    <tr>
                    <th>Tanggal Rilis</th>
                    <td>{{ $berita->tanggal_berita}}</td>
                    </tr>
                    <tr>
                    <th>Penulis</th>
                    <td>{{ $berita->penulis_berita}}</td>
                    </tr>
                    <tr>
                    <th>Gambar Berita</th>
                    <td>
                        <a href="{{ asset('/public/beritaupload/'.$berita->gambar_berita) }}"  data-toggle="lightbox" >
                        <img id="preview_photo" src="{{ asset('/public/beritaupload/'.$berita->gambar_berita) }}"  class="img-fluid mb-2" alt="white sample"/>
                    </td>
                    </tr>
                    <tr>
                    <th>Isi Berita</th>
                    <td>{{ $berita->isi_berita}}</td>
                    </tr>
                    <tr>    
                    </thead>
                  </table>
                </div>
                <div class="card-footer text-right">
                <a class="btn btn-primary" href="{{ url('cp/show_berita') }}">
                Back
            </a>
        </div>
            </div>
          </section>
@stop