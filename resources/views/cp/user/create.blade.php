@extends('layouts/master')
@section('content')

<form action="{{ url('cp/user/save') }}" method="POST" enctype="multipart/form-data">
@csrf
<body>

    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="page-title mb-0 p-0">Tambah Data User</h3>
                    </div>
                </div>
            </div>
            @include('errors/form_error')
            <div class="container-fluid">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal form-material mx-2">
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 mb-0">Nama</label>
                                        <div class="col-md-12">
                                            <input type="text" name="name" value="{{ old('name') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-md-12 mb-0">Email</label>
                                        <div class="col-md-12">
                                            <input type="text" name="email" value="{{ old('email') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                            <div class="col-md-12">
                                            <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control ps-0 form-control-line">
                                            </div>             
                                    </div>
                                    <div class="form-group">
                                        <label  for="gambar" class="col-sm-6 control-label">Gambar</label>
                                        <img id="preview_photo" class="img-fluid md-3 mb-3"  />
                                        <div class="col-md-12">
                                        <input type="file" accept="image/*" name="gambar" onchange="preview(event)"  value="{{ old('gambar') }}" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="point" class="col-md-12 mb-0">Point</label>
                                        <div class="col-md-12">
                                        <input type="text" name="ponit" value="{{ old('ponit') }}" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 d-flex">
                                            <input class="btn btn-success mx-auto mx-md-0 text-white" type="submit" value="Submit" class="primary" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>        
        </div>
    </div>


</body>
</form>
@stop

{{-- JS untuk menampilkam photo profile --}}
<script type="text/javascript">
        function preview(event){
            var input = event.target.files[0];
            var reader = new FileReader();

            reader.onload = function(){
                var result = reader.result;
                var preview_photo = document.getElementById('preview_photo');
                preview_photo.src = result
            }

            reader.readAsDataURL(input);
        }
    </script>