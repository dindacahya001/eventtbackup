
@extends('layouts/master')

@section('content')
<div id="kuliner">

<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Detail User</h3>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                    <th>Gambar</th>
                    <td>
                        <a href="{{ asset('/public/userupload/'.$user->gambar) }}"  data-toggle="lightbox" >
                        <img id="preview_photo" src="{{ asset('/public/userupload/'.$user->gambar) }}"  class="img-fluid mb-2" alt="white sample"/>
                    </td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td>{{ $user->name}}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $user->email}}</td>
                    </tr>
                    <tr>
                        <th>Password</th>
                        <td>{{ $user->password}}</td>
                    </tr>
                    <tr>
                        <th>Point</th>
                        <td>{{ $user->point}}</td>
                    </tr>
                    <tr>    
                    </thead>
                  </table>
                </div>
                <div class="card-footer text-right">
                <a class="btn btn-primary" href="{{ url('cp/show_user') }}">
                Back
            </a>
        </div>
            </div>
          </section>
@stop