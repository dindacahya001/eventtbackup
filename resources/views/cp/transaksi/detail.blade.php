
@extends('layouts/master')

@section('content')
<div id="kuliner">

<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Detail Transaksi</h3>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Judul Event</th>
                        <td>{{ $transaksi->event['judul_event']}}</td>
                    </tr>
                    <tr>
                        <th>User</th>
                        <td>{{ $transaksi->user_id}}</td>
                    </tr>
                    <tr>
                    <th>Bukti Pembayaran</th>
                    <td>
                        <a href="{{ asset('/public/transaksiupload/'.$transaksi->bukti) }}"  data-toggle="lightbox" >
                        <img id="preview_photo" src="{{ asset('/public/transaksiupload/'.$transaksi->bukti) }}"  class="img-fluid mb-2" alt="white sample"/>
                    </td>
                    </tr>
                    <tr>
                        <th>Total Harga</th>
                        <td>{{$transaksi->tot_harga}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{ $transaksi->status}}</td>
                    </tr>
                    </thead>
                  </table>
                </div>
                <div class="card-footer text-right">
                <a class="btn btn-primary" href="{{ url('cp/show_transaksi') }}">
                Back
            </a>
        </div>
            </div>
          </section>
@stop