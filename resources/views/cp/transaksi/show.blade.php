@extends('layouts/master')

@section('content')
<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="page-title mb-0 p-0">Data Transaksi</h3>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!-- column -->                   
                <div class="card">
                    <div class="col-sm-12">
                    @if (!empty($transaksi_list))
                        <div class="card">
                        @if (session('sukses'))
                        <div class="alert alert-warning" role="alert">
                            {{session('sukses')}}
                        </div>
                            @endif
                            <div class="card-body">
                            @if (!empty($transaksi_list))
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0" style="text-align:center" width="80px">Judul event</th>
                                                <th class="border-top-0" style="text-align:center" width="80px">User</th>
                                                <th class="border-top-0" style="text-align:center" width="80px">Status</th>
                                                <th class="border-top-0" style="text-align:center" width="40px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($transaksi_list as $transaksi)
                                            <tr>
                                                <td>{{ $transaksi->event['judul_event']}} </td>
                                                <td>{{ $transaksi->user_id}} </td>
                                                <td>{{ $transaksi->status}} </td>
                                                <td style="text-align:center" class="project-actions text-right"> 
                                                <a class="btn btn-primary btn-sm" href="{{ url('cp/detail_transaksi/'.$transaksi->id) }}">
                                                    <i class="fas fa-folder">
                                                    </i>
                                                    Detail
                                                </a>
                                                <a class="btn btn-danger btn-sm" href ="{{ url('cp/hapus_transaksi/'.$transaksi->id) }}" >
                                                    <i class="fas fa-trash">
                                                    </i>
                                                    Delete
                                                </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="card-footer clearfix">
                                        <ul class="pagination pagination-sm m-0 float-right">
                                        <div class="paging">
                                        {{ $transaksi_list->links() }}
                                         </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@stop   

