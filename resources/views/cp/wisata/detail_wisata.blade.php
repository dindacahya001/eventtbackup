
@extends('layouts/master')

@section('content')
<div id="wisata">

<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Detail Wisata</h3>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Ringkasan</th>
                        <td>{{ $wisata->ringkasan}}</td>
                    </tr>
                    <tr>
                    <th>Gambar</th>
                    <td>
                        <a href="{{ asset('/public/wisataupload/'.$wisata->gambar_wisata) }}"  data-toggle="lightbox" >
                        <img id="preview_photo" src="{{ asset('/public/wisataupload/'.$wisata->gambar_wisata) }}"  class="img-fluid mb-2" alt="white sample"/>
                    </td>
                    </tr>
                    <tr>
                    <th>Maps</th>
                    <td>
                        <div class="border-top-0" style="text-align:center" height="50px">
                        {!! $wisata->maps!!}
                        </div>
                    </td>
                    </tr>
                    <tr>    
                    </thead>
                  </table>
                </div>
                <div class="card-footer text-right">
                <a class="btn btn-primary" href="{{ url('cp/show_wisata') }}">
                Back
            </a>
        </div>
            </div>
          </section>
@stop