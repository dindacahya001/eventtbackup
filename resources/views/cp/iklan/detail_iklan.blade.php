
@extends('layouts/master')

@section('content')
<div id="iklan">

<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Detail Iklan</h3>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Judul</th>
                        <td>{{ $iklan->judul_iklan}}</td>
                    </tr>  
                    <tr>
                    <th>Gambar</th>
                    <td>
                        <a href="{{ asset('/public/iklanupload/'.$iklan->gambar_iklan) }}"  data-toggle="lightbox" >
                        <img id="preview_photo" src="{{ asset('/public/iklanupload/'.$iklan->gambar_iklan) }}"  class="img-fluid mb-2" alt="white sample"/>
                    </td>
                    </tr>
                    <tr>
                        <th>Ringkasan</th>
                        <td>{{ $iklan->ringkasan}}</td>
                    </tr>   
                    </thead>
                  </table>
                </div>
                <div class="card-footer text-right">
                <a class="btn btn-primary" href="{{ url('cp/show_iklan') }}">
                Back
            </a>
        </div>
            </div>
          </section>
@stop