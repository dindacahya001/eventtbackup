<!DOCTYPE html>
<html lang="en">
  <body >
  @include('layouts.master')
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="page-title mb-0 p-0">Dashboard</h3>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                   
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Transaksi</h4>
                                <i style="font-size: 30px" class="me-3 fas fa-cart-plus"></i>
                                <div class="text-end">
                                
                                    <h2 style="font-size: 45px">{{$count ['transaksi'] }}</h2>
                                    </div>
                                        <a href="{{url('cp/show_transaksi')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                    
                    <!-- Column -->
                    <!-- Column -->
                    
                    <div class="col-lg-4 col-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Event</h4>
                                <i style="font-size: 30px" class="me-3 fa fa-table"></i>
                                <div class="text-end">
                                    <h2 style="font-size: 45px"> {{$count ['event'] }}</h2>
                                    </div>
                                        <a href="{{url('cp/show_event')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                    <!-- Column -->
               
                
                    <!-- Column -->
                    <div class="col-lg-4 col-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Wisata</h4>
                                <i style="font-size: 30px" class="me-3 fa fa-globe"></i>
                                <div class="text-end">
                                    <h2 style="font-size: 45px"> {{$count ['wisata'] }}</h2>
                                    </div>
                                        <a href="{{url('cp/show_wisata')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                   
                    <!-- Column -->
                    <!-- Column -->
                    
                    <div class="col-lg-4 col-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Kuliner</h4>
                                <i style="font-size: 30px" class="me-3 fa fa-coffee"></i>
                                <div class="text-end">
                                    <h2 style="font-size: 45px"> {{$count ['kuliner'] }}</h2>
                                    </div>
                                        <a href="{{url('cp/show_kuliner')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                    <!-- Column -->
              
                    <!-- Column -->
                    <div class="col-lg-4 col-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Berita</h4>
                                <i style="font-size: 30px" class="me-3 fa fa-font"></i>
                                <div class="text-end">
                                    <h2 style="font-size: 45px"> {{$count ['berita'] }} </h2>
                                    </div>
                                        <a href="{{url('cp/show_berita')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                   
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Iklan</h4>
                                <i style="font-size: 30px" class="me-3 fas fa-tv"></i>
                                <div class="text-end">
                                    <h2 style="font-size: 45px"> {{$count ['event'] }}</h2>
                                    </div>
                                        <a href="{{url('cp/show_iklan')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                   
                    <!-- Column -->
                </div>
            </div>
            
             
        </div>  
  </body>
</html>