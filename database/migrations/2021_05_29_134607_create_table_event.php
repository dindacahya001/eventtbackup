<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->id();
            $table->text('judul_event');
            $table->text('tempat_event');
            $table->string('tanggal_event');
            $table->time('waktu_event');
            $table->string('gambar_event');
            $table->text('harga');
            $table->text('tata_cara');
            $table->text('ringkasan');
            $table->text('maps');
            $table->enum('category', ['rekomendasi', 'update']);
            $table->timestamps();
        });

        Schema::table('transaksi', function (Blueprint $table) {
            $table->foreign('event_id')
            ->references('id')
            ->on('event')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event', function (Blueprint $table){
            $table->dropForeign('transaksi_event_id_foreign');
        });

        Schema::dropIfExists('event');
    }
}
