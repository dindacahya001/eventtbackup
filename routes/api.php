<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::group([
//     'prefix' => 'api',
// 	'namespace' => 'api',
// 	// 'middleware' => 'auth',
// 	// 'as' => 'Api.'
// ], function () {
//     Route::get('show_berita', 'Api\BeritaApiController@index');
// });

//berita
Route::get('show_berita', 'Api\BeritaApiController@index');
Route::get('detail_berita/{berita}', 'Api\BeritaApiController@detail_berita');

//wisata
Route::get('show_wisata', 'Api\WisataApiController@index');
Route::get('detail_wisata/{wisata}', 'Api\WisataApiController@detail_wisata');

//kuliner
Route::get('show_kuliner', 'Api\KulinerApiController@index');
Route::get('detail_kuliner/{kuliner}', 'Api\KulinerApiController@detail_kuliner');

//event
Route::get('show_event', 'Api\EventApiController@index');
Route::get('detail_event/{event}', 'Api\EventApiController@detail_event');

//user
Route::get('show_user', 'Api\UserApiController@index');
Route::put('user/update/{id}', 'Api\UserApiController@update');

//transaksi
Route::get('show_transaksi', 'Api\TransaksiApiController@index');
Route::put('transaksi/update/{id}', 'Api\TransaksiApiController@update');
Route::get('hapus_transaksi/{id}','Api\TransaksiApiController@delete');

//iklan
Route::get('show_iklan', 'Api\IklanApiController@index');
Route::get('detail_iklan/{iklan}', 'Api\IklanApiController@detail_iklan');
