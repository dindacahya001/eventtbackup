<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group([
    'prefix' => 'cp',
	'namespace' => 'Cp',
	'middleware' => 'auth',
	'as' => 'cp.'
], function () {

	Route::get('/', 'PagesController@index')->name('home');

//berita
Route::post('berita/save', 'BeritaController@store');
Route::get('create_berita', 'BeritaController@create_berita');
Route::get('show_berita', 'BeritaController@show_berita');
Route::get('/edit_berita/{berita}/edit', 'BeritaController@edit');
Route::put('berita/update/{id}', 'BeritaController@update');
Route::get('hapus_berita/{id}', 'BeritaController@delete');
Route::get('detail_berita/{berita}', 'BeritaController@detail_berita');

//wisata
Route::post('wisata/save', 'WisataController@store');
Route::get('create_wisata', 'WisataController@create');
Route::get('show_wisata', 'WisataController@show');
Route::get('/edit_wisata/{wisata}/edit', 'WisataController@edit');
Route::put('wisata/update/{id}', 'WisataController@update');
Route::get('hapus_wisata/{id}', 'WisataController@delete');
Route::get('detail_wisata/{wisata}', 'WisataController@detail_wisata');

//kuliner
Route::post('kuliner/save', 'KulinerController@store');
Route::get('create_kuliner', 'KulinerController@create');
Route::get('show_kuliner', 'KulinerController@show');
Route::get('/edit_kuliner/{kuliner}/edit', 'KulinerController@edit');
Route::put('kuliner/update/{id}', 'KulinerController@update');
Route::get('hapus_kuliner/{id}', 'KulinerController@delete');
Route::get('detail_kuliner/{kuliner}', 'KulinerController@detail_kuliner');

//event
Route::post('event/save', 'EventController@store');
Route::get('create_event', 'EventController@create');
Route::get('show_event', 'EventController@show');
Route::get('/edit_event/{event}/edit', 'EventController@edit');
Route::put('event/update/{id}', 'EventController@update');
Route::get('hapus_event/{id}', 'EventController@delete');
Route::get('detail_event/{event}', 'EventController@detail_event');

//user
Route::get('create_user', 'UserController@create');
Route::get('show_user', 'UserController@show');
Route::post('user/save', 'UserController@store');
Route::get('edit_user/{user}/edit', 'UserController@edit');
Route::put('user/update/{id}', 'UserController@update');
Route::get('hapus_user/{id}','UserController@delete');
Route::get('detail_user/{user}', 'UserController@detail');

//transaksi
Route::get('create_transaksi', 'TransaksiController@create');
Route::post('transaksi/save', 'TransaksiController@store');
Route::get('show_transaksi', 'TransaksiController@show');
Route::get('hapus_transaksi/{id}','TransaksiController@delete');
Route::get('detail_transaksi/{transaksi}', 'TransaksiController@detail');

//iklan
Route::post('iklan/save', 'IklanController@store');
Route::get('create_iklan', 'IklanController@create');
Route::get('show_iklan', 'IklanController@show');
Route::get('/edit_iklan/{iklan}/edit', 'IklanController@edit');
Route::put('iklan/update/{id}', 'IklanController@update');
Route::get('hapus_iklan/{id}', 'IklanController@delete');
Route::get('detail_iklan/{iklan}', 'IklanController@detail_Iklan');
});



